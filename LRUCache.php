<?php

class Node {
  public $key;
  public $value;
  public $next;
  public $previous;
}

class LRUCache {

  /**
   * @var array
   *   The elements with a key->value
   */
  protected $hashed_elements;

  /**
   * @var array
   *   The elements in most recent order (only sets key to TRUE);
   */
  protected $most_recent_elements;

  /**
   * @var int
   *   Max allowed memory for all data.
   */
  protected $allowed_memory_limit;

  /**
   * @var int
   *   Current estimated size of data.
   */
  protected $current_size;

  /**
   * @var int
   *   Base size of data structures.
   */
  protected $base_size;

  /**
   * LRUCache constructor.
   * @param int $max_allowed_size
   *   The max allowed size (in bytes).
   */
  public function __construct(int $max_allowed_size) {
    $this->hashed_elements = [];
    $this->most_recent_elements = [];
    $this->current_size = $this->base_size = 5;
    // In large instances, php overhead may be great. Try at half point.
    $this->allowed_memory_limit = max($this->base_size, $max_allowed_size / 2);
  }

  /**
   * Returns the cached data at requested key.
   * @param string $key
   *   The key
   * @return bool|mixed
   *   The value at key. FALSE otherwise.
   */
  public function get($key) {
    if (!empty($this->hashed_elements[$key])) {
      unset($this->most_recent_elements[$key]);
      $this->most_recent_elements[$key] = TRUE;
      return $this->hashed_elements[$key];
    }
    return FALSE;
  }

  /**
   * Sets value of item at key.
   * @param string $key
   * @param string $value
   */
  public function set($key, $value) {
    $this->delete($key);
    $this->current_size += (strlen($key) * 2) + strlen($value);
    if ($this->current_size > $this->allowed_memory_limit) {
      foreach ($this->most_recent_elements as $element_key => $element_value) {
        $this->current_size = $this->current_size - (strlen($element_key) * 2) - strlen($element_value);
        unset($this->most_recent_elements[$element_key]);
        unset($this->hashed_elements[$element_key]);
        if ($this->current_size <= $this->allowed_memory_limit) {
          break;
        }
      }
    }
    $this->hashed_elements[$key] = $value;
    $this->most_recent_elements[$key] = TRUE;
  }

  /**
   * Deletes key-value from cache.
   * @param string $key
   *
   */
  public function delete($key) {
    if ($this->exists($key)) {
      $this->current_size = $this->current_size - (strlen($key) * 2) - strlen($this->hashed_elements[$key]);
      unset($this->most_recent_elements[$key]);
      unset($this->hashed_elements[$key]);
    }
  }

  /**
   * @param string $key
   *   The key in cache.
   * @return bool
   *   TRUE if element exists. FALSE otherwise.
   */
  public function exists($key) {
    return !empty($this->hashed_elements[$key]);
  }

  /**
   * @return int
   *   Number of elements in array.
   */
  public function getElementCount() {
    return count($this->hashed_elements);
  }

  /**
   * @return int
   *   Total estimated size of data.
   */
  public function getSize() {
    return $this->current_size;
  }
}
