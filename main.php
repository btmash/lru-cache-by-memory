<?php

require_once 'LRUCache.php';

function generateRandomString($length = 10) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}

function assertExists(LRUCache $cache, $key) {
  if (!$cache->exists($key)) {
    throw new Exception('Key ' . $key . ' with value not found');
  }
}

function assertNotExists(LRUCache $cache, $key) {
  if ($cache->get($key) !== FALSE) {
    throw new Exception('Key ' . $key . ' found with value ' . $cache->get($key));
  }
}

function test_small_cache() {
  $cache = new LRUCache('35');
  $cache->set('ashok', 'test');
  assertExists($cache, 'ashok');
  $cache->set('sam', 'founder');
  assertExists($cache, 'sam');
  $cache->set('chris', 'engineer');
  assertExists($cache, 'chris');
  $cache->set('sam', 'founder of chaos');
  assertExists($cache, 'sam');
  assertNotExists($cache, 'chris');
}

function test_large_cache() {
  $memory_limit = 128;
  @ini_set('memory_limit', $memory_limit . 'M');

  $cache = new LRUCache($memory_limit * 1024 * 1024);
  for ($i = 0; $i < 1000000; $i++) {
    var_dump($i);
    $key = generateRandomString(rand(10, 30));
    $value = generateRandomString(rand(10, 500));
    $cache->set($key, $value);
  }
  var_dump($cache->getElementCount());
  var_dump($cache->getSize());
}

test_small_cache();
var_dump(memory_get_usage());
test_large_cache();
var_dump(memory_get_peak_usage());
